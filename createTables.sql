CREATE TABLE Roles(
	id INTEGER NOT NULL PRIMARY KEY,
	name nvarchar(64)
);

CREATE TABLE Orders(
	id INTEGER NOT NULL PRIMARY KEY,
	firstName nvarchar(128) NOT NULL,
	lastName nvarchar(128) NOT NULL,
	email nvarchar(320) NOT NULL,
	phoneNumber INTEGER NOT NULL,
	price INTEGER NOT NULL
);

CREATE TABLE OrderContent(
	id INTEGER NOT NULL PRIMARY KEY,
	orderID INTEGER NOT NULL,
	productID INTEGER NOT NULL,
	userID INTEGER NOT NULL,
	dateTime INTEGER NOT NULL
);

CREATE TABLE Users(
	id INTEGER NOT NULL PRIMARY KEY,
	roleID INTEGER NOT NULL,
	username nvarchar(50) NOT NULL,
	password nvarchar(255) NOT NULL,
	email nvarchar(320) NOT NULL
);

CREATE TABLE Categories(
	id INTEGER NOT NULL PRIMARY KEY,
	name nvarchar(128) NOT NULL
);

CREATE TABLE Products(
    id INTEGER NOT NULL PRIMARY KEY,
	name nvarchar(128) NOT NULL,
	categoryID INTEGER NOT NULL,
	price INTEGER NOT NULL,
	shortDescription nvarchar(64) NOT NULL,
	description nvarchar(256) NOT NULL,
	time INTEGER NOT NULL
);