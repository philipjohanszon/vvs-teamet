const port = 4998;
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
require('dotenv').config();
const stripe = require("stripe")(process.env.STRIPE_API_KEY);
const endpointSecret = process.env.ENDPOINT_SECRET;

//OWN IMPORTS
const db = require("./libs/database.js");
const { Order } = require("./libs/order.js");

app.use((req, res, next) => {
    if (req.originalUrl === '/webhook') {
        next();
    } else {
        bodyParser.json()(req, res, next);
    }
});

app.use(express.static(__dirname + '/public'));

app.use(express.urlencoded());
app.use(express.json());

app.get("/", (req, res) => {
    res.sendFile("./html/frontpage.html", { root: __dirname });
});

app.get("/shop", (req, res) => {
    res.sendFile("./html/shop.html", { root: __dirname });
});

app.get("/orderBekraftning", (req, res) => {
    res.sendFile("./html/confirmOrder.html", { root: __dirname });
});

app.get("/hitta", (req, res) => {
    res.sendFile("./html/findUs.html", { root: __dirname });
});

app.get("/kontakta", (req, res) => {
    res.sendFile("./html/contactUs.html", { root: __dirname });
});

app.get("/termsandconditions", (req, res) => {
    res.sendFile("./html/termsandconditions.html", { root: __dirname });
});

app.get("/lyckad", (req, res) => {
    res.sendFile("./html/success.html", { root: __dirname });
});

app.get("/misslyckad", (req, res) => {
    res.sendFile("./html/fail.html", { root: __dirname });
});

app.post("/getProducts", (req, res) => {
    const categoryID = req.body.categoryID;

    const sql = "SELECT Products.id, Products.name, Products.shortDescription, Products.description, Products.price  From Products INNER JOIN Categories WHERE Products.categoryID = Categories.id AND Categories.id = ?";
    let query = db.querySelect(sql, categoryID);

    for (let i = 0; i < query.length; i++) {
        query[i].smallImgSrc = "./productImages/small/" + query[i].id.toString() + ".jpg";
        query[i].bigImgSrc = "./productImages/big/" + query[i].id.toString() + ".jpg";
    }

    res.send(query);
});

app.post("/getCategories", (req, res) => {
    const sql = "SELECT id, name FROM Categories";
    let query = db.querySelect(sql, []);

    res.send(query);
});

app.post("/getProductInfo", (req, res) => {
    /*
        product: id
        products: [id, id, id, id, id]
    */

    let sql = "";
    let query;

    //Multiple ids
    if (req.body.products) {
        let products = JSON.parse(req.body.products);

        sql = "SELECT Products.id, Products.name, Products.shortDescription, Products.description, Products.price, Products.categoryID From Products WHERE ";

        for (let i = 0; i < products.length; i++) {
            if (i === 0) {
                sql += "Products.id = ? "
            }
            else {
                sql += "OR Products.id = ? "
            }
        }

        query = db.querySelect(sql, products);
    }
    //single id
    else if (req.body.product) {
        let product = req.body.product;

        sql = "SELECT Products.id, Products.name, Products.shortDescription, Products.description, Products.price, Products.categoryID From Products WHERE Products.id = ?"
        query = db.querySelect(sql, product);
    }

    res.send(query);
});


//STRIPE

app.post('/create-checkout-session', async (req, res) => {

    let order = new Order(req.body.cart);

    if (order != false) {
        const session = await stripe.checkout.sessions.create({
            payment_method_types: ['card'],
            line_items: order.generateLineItems(),
            /*[
                {
                    price_data: {
                        currency: 'sek',
                        product_data: {
                            name: 'T-shirt',
                        },
                        unit_amount: 2000,
                    },
                    quantity: 1
                }
            ]*/
            mode: 'payment',
            success_url: 'https://vvsteamet.philipjohanszon.se/lyckad',
            cancel_url: 'https://vvsteamet.philipjohanszon.se/misslyckad',
            metadata: {
                firstName: req.body.contactInfo.firstName,
                lastName: req.body.contactInfo.lastName,
                email: req.body.contactInfo.email,
                phoneNr: req.body.contactInfo.phoneNr,
                items: JSON.stringify(order.getCartContent()),
                totalPrice: order.getTotalPrice()
            }

        });


        res.json({ id: session.id });
    }
});

app.post('/webhook', bodyParser.raw(), (req, res) => {
    const sig = req.headers['stripe-signature'];
    let event;

    try {
        //TO DO CHECK THAT IT ISNT FORGED
        event = req.body;
    } catch (err) {
        res.status(400).send(`Webhook Error: ${err.message}`);
    }


    // Handle the event
    switch (event.type) {
        case 'payment_intent.succeeded':
            const paymentIntent = event.data.object;
            console.log('PaymentIntent was successful!');
            break;
        case "checkout.session.completed":
            const metadata = event.data.object.metadata;
            let order = JSON.parse(metadata.items);

            console.log(metadata);

            break;
        case 'payment_method.attached':
            const paymentMethod = event.data.object;
            console.log('PaymentMethod was attached to a Customer!');
            break;
        // ... handle other event types
        default:
            console.log(`Unhandled event type ${event.type}`);
    }

    // Return a 200 response to acknowledge receipt of the event
    res.json({ received: true });
});


app.listen(port);
