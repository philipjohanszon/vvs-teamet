const Database = require('better-sqlite3');
const db = new Database('database.db');

function query(sql, params) {
    return db.prepare(sql).run(params).changes;
}

function querySelect(sql, params) {
    return db.prepare(sql).all(params);
}

module.exports = { query, querySelect } 