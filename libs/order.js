const db = require("./database.js");

class Order {
    constructor(orderItems) {
        this.orderItems = orderItems;

        for (let i = 0; i < this.orderItems.length; i++) {
            if (this.orderItems[i].id == undefined || this.orderItems[i].amount == undefined) {
                return false;
            }
        }
    }

    calculateOrderTotal() {
        let total;

        for (let i = 0; i < this.orderItems.length; i++) {
            total += this.orderItems[i].price * this.orderItems[i].amount;
        }

        return total;
    }

    generateLineItems() {
        let lineItems = [];

        let sql = "SELECT id, name, price FROM Products WHERE ";
        let params = [];

        for (let i = 0; i < this.orderItems.length; i++) {
            if (i === 0) {
                sql += "id = ? "
            }
            else {
                sql += "OR id = ? ";
            }

            params.push(this.orderItems[i].id);
        }

        let query = db.querySelect(sql, params);


        for (let i = 0; i < this.orderItems.length; i++) {
            for (let j = 0; j < query.length; j++) {
                if (query[j].id === this.orderItems[i].id) {
                    const obj = {
                        price_data: {
                            currency: "sek",
                            product_data: {
                                name: query[j].name
                            },
                            unit_amount: query[j].price * 100 // *100 because it is displayed in öre
                        },
                        quantity: this.orderItems[i].amount
                    }

                    lineItems.push(obj);
                }
            }

        }

        return lineItems;
    }

    getTotalPrice() {
        let total = 0;

        for (let i = 0; i < this.orderItems.length; i++) {
            total += this.orderItems[i].price * this.orderItems[i].amount;
        }

        return total;
    }

    getCartContent() {
        let cart = [];

        for (let i = 0; i < this.orderItems.length; i++) {
            cart.push({ id: this.orderItems[i].id, amount: this.orderItems[i].amount });
        }

        return cart;
    }
}

module.exports = { Order };