const shopConfirm = {
    data() {
        return {
            cart: [],
            totalPrice: 0,
            agreedToTerms: false,
            firstName: "",
            lastName: "",
            email: "",
            phoneNr: 0,
            modalOpen: false,
            stripe: Stripe('pk_test_51HvR0KAEhEbsQxE2nwCrDlIgnvvwF8DrnPzCrV0PbShVMpZH6yOuTbE4oLDCFvnbOpZyaH6I6v6sZMx98l2Y4D4y00pt4e01ch')
        }
    },
    methods: {
        calculateTotalPrice() {
            let total = 0;

            for (let i = 0; i < this.cart.length; i++) {
                total = total + this.cart[i].amount * this.cart[i].price;
            }

            this.totalPrice = total;
        },

        addItem(id) {

            let localCart = JSON.parse(localStorage.getItem("cart"));

            for (let i = 0; i < localCart.length; i++) {
                if (localCart[i].id === id) {
                    localCart[i].amount += 1;
                }
            }

            localStorage.setItem("cart", JSON.stringify(localCart));

            for (let i = 0; i < this.cart.length; i++) {
                if (this.cart[i].id === id) {
                    this.cart[i].amount += 1;
                }
            }

            this.calculateTotalPrice();
        },

        removeItem(id) {

            let localCart = JSON.parse(localStorage.getItem("cart"));

            for (let i = 0; i < localCart.length; i++) {
                if (localCart[i].id === id) {
                    localCart[i].amount -= 1;

                    //Removes the item if amount < 1
                    if (localCart[i].amount < 1) {
                        localCart.splice(i, 1);
                    }
                }
            }

            localStorage.setItem("cart", JSON.stringify(localCart));

            for (let i = 0; i < this.cart.length; i++) {
                if (this.cart[i].id === id) {
                    this.cart[i].amount -= 1;

                    //Removes the item if amount < 1
                    if (this.cart[i].amount < 1) {
                        this.cart.splice(i, 1);
                    }
                }
            }

            this.calculateTotalPrice();

        },

        checkOut() {

            if (this.agreedToTerms && this.email.length > 0 && this.firstName.length > 0 && this.lastName.length > 0 && this.phoneNr > 0) {
                let self = this;

                console.log(this.cart);

                fetch('/create-checkout-session', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ cart: this.cart, contactInfo: { firstName: this.firstName, lastName: this.lastName, email: this.email, phoneNr: this.phoneNr } })
                })
                    .then(function (response) {
                        return response.json();
                    })
                    .then(function (session) {
                        return self.stripe.redirectToCheckout({ sessionId: session.id });
                    })
                    .then(function (result) {
                        // If `redirectToCheckout` fails due to a browser or network
                        // error, you should display the localized error message to your
                        // customer using `error.message`.
                        if (result.error) {
                            alert(result.error.message);
                        }
                    })
                    .catch(function (error) {
                        console.error('Error:', error);
                    });
            }
            else {
                alert("Du måste godkänna villkoren och fylla i alla rutor!");
            }


        }
    },
    mounted() {
        const cart = JSON.parse(localStorage.getItem("cart"));
        let self = this;

        if (cart != null) {
            let modifiedCart = [];

            if (cart.length > 1) {

                for (let i = 0; i < cart.length; i++) {
                    console.log(cart[i])
                    modifiedCart.push(cart[i].id);
                }

                console.log(modifiedCart);

                $.post("/getProductInfo", { products: JSON.stringify(modifiedCart) }, function (data) {
                    let newCart = data;

                    for (let i = 0; i < cart.length; i++) {
                        for (let j = 0; j < newCart.length; j++) {
                            if (cart[i].id === newCart[j].id) {
                                newCart[j].amount = cart[i].amount;
                            }
                        }
                    }

                    console.log("multi: ", newCart)
                    self.cart = newCart;

                    self.calculateTotalPrice();


                });
            }
            else if (cart.length === 1) {
                $.post("/getProductInfo", { product: cart[0].id, name: true, price: true, id: true }, (data) => {
                    let newCart = data;
                    newCart[0].amount = cart[0].amount;
                    console.log("solo: ", newCart);
                    self.cart = newCart;

                    self.calculateTotalPrice();

                });
            }
        }


    }
}

Vue.createApp(shopConfirm).mount('#shopConfirm');

