const shop = {
    data() {
        return {
            selectedCategory: 0,
            loaded: false,
            modalOpen: false,
            categories: [

            ],
            selectedProduct: [
                { id: -1, name: "NULL", imgSrc: "#", shortDescription: "NULL", price: 0 }
            ]
            /*categories: [
                {
                    id: 1, name: "WC-stolar", products: [
                        { id: 1, name: "toastol, norges bästa", imgSrc: "/productImages/small/1.png", shortDescription: "Den bästa toastolen norge har skapat, har visats vara bäst i test. 10/10", price: 2999 },
                        { id: 2, name: "toastol2, norges bästa", imgSrc: "/productImages/small/2.png", shortDescription: "Den bästa toastolen norge har skapat, har visats vara bäst i test. 11/10", price: 3399 }
                    ]
                },
                {
                    id: 2, name: "badkar", products: [
                        { id: 3, name: "badkar, norges bästa", imgSrc: "/productImages/small/3.png", shortDescription: "Den bästa toastolen norge har skapat, har visats vara bäst i test. 10/10", price: 899 },
                        { id: 4, name: "badkar2, norges bästa", imgSrc: "/productImages/small/4.png", shortDescription: "Den bästa badkar norge har skapat, har visats vara bäst i test. 11/10", price: 999 },
                        { id: 5, name: "badkar3, norges bästa", imgSrc: "/productImages/small/5.png", shortDescription: "Den bästa trumpet norge har skapat, har visats vara bäst i test. 11/10", price: 299 }
                    ]
                },
                {
                    id: 3, name: "handfat", products: [
                        { id: 6, name: "handfat, norges bästa", imgSrc: "/productImages/small/6.png", shortDescription: "Den bästa handfat norge har skapat, har visats vara bäst i test. 10/10", price: 8999 },
                        { id: 7, name: "handfat2, norges bästa", imgSrc: "/productImages/small/7.png", shortDescription: "Den bästa handfat2 norge har skapat, har visats vara bäst i test. 11/10", price: 9999 },
                        { id: 8, name: "handfat3, norges bästa", imgSrc: "/productImages/small/8.png", shortDescription: "Den bästa handfat3 norge har skapat, har visats vara bäst i test. 11/10", price: 29999 },
                        { id: 9, name: "handfat4, norges bästa", imgSrc: "/productImages/small/9.png", shortDescription: "Den bästa handfat4 norge har skapat, har visats vara bäst i test. 11/10", price: 2999 }
                    ]
                }
            ]*/

        }
    },

    methods: {

        switchCategory() {
            this.loaded = false;
            this.loadProducts(this.categories[this.selectedCategory].id);
            console.log(this.selectedCategory);
        },

        loadProducts(categoryID) {
            let self = this;
            $.post("/getProducts", { categoryID: categoryID }, function (productsData) {
                for (let i = 0; i < self.categories.length; i++) {
                    if (self.categories[i].id === categoryID) {
                        self.categories[i].products = productsData;
                    }
                }

                self.loaded = true;
            });
        },

        showProduct(id) {
            console.log(id)
            for (let i = 0; i < this.categories[this.selectedCategory].products.length; i++) {
                console.log(this.categories[this.selectedCategory].products[i].id)
                if (this.categories[this.selectedCategory].products[i].id === id) {
                    this.selectedProduct[0] = this.categories[this.selectedCategory].products[i];
                }
            }

            this.modalOpen = true;
        },

        addToCart(id, productName) {

            let cart = localStorage.getItem("cart");

            if (cart === null) {
                localStorage.setItem("cart", JSON.stringify([{ id: id, name: productName, amount: 1 }]));
            }
            else {
                let parsedCart = JSON.parse(cart);

                let found = false;

                for (let i = 0; i < parsedCart.length; i++) {
                    if (parsedCart[i].id === id) {
                        parsedCart[i].amount++;
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    parsedCart.push({ id: id, name: productName, amount: 1 });
                }

                localStorage.setItem("cart", JSON.stringify(parsedCart));
            }
            //Close modal
            this.modalOpen = false;
        }
    },
    mounted() {
        let self = this;
        $.post("/getCategories", function (categoryData) {
            console.log(categoryData);
            $.post("/getProducts", { categoryID: categoryData[0].id }, function (productsData) {
                categoryData[0].products = productsData;
                self.categories = categoryData;
                self.loaded = true;
            });
        });
    }
};

Vue.createApp(shop).mount('#shop');